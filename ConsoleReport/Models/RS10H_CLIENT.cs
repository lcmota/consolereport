﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleReport.Models
{
   public class RS10H_CLIENT
    {
            public int ID { get; set; }
            public int ID_CLIENT { get; set; }
            public Nullable<int> ID_CLIENT_USER { get; set; }
            public int ID_CONFIG { get; set; }

            public virtual RS10X_CONFIG RS10X_CONFIG { get; set; }

    }
}
