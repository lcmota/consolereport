﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleReport;
using ConsoleReport.WSReport;
using System.IO;
using System.Net;
using System.Configuration;
using System.Data.SqlClient;
using Dapper;
using Newtonsoft.Json;
using System.Net.Mail;
using System.Globalization;

namespace ConsoleReport
{
    class Program
    {
        static string connDev = ConfigurationManager.ConnectionStrings["SqlServerDEV"].ConnectionString;
        static string conn = ConfigurationManager.ConnectionStrings["SqlServer"].ConnectionString;

        static void Main(string[] args)
        {
          
            if (args.Length == 0)
                startreports();
            else
                startreports(Convert.ToInt32(args[0]));
        }

        private static void startreports(int IdTipoRel = -1)
        {
            //getreports from bd

            //ReprotConfig rconf = new ReprotConfig();


            IList<CLS_REPORTS> reports = new List<CLS_REPORTS>();
            using (var sqlConnection = new SqlConnection(connDev))
            {
                sqlConnection.Open();
                string query = @"
                                     select * 
                                       from inReports.dbo.RS10X_CONFIG xconfig (nolock)
                                       join inReports.dbo.RS10H_CLIENT hclient (nolock) on hclient.ID_CONFIG = xconfig.ID_CONFIG
                                       join inReports.dbo.RS10D_FTP ftp  (nolock) on ftp.ID_FTP = xconfig.ID_FTP
                                       join inReports.dbo.RS10D_TIPOREL tiporel (nolock) on tiporel.ID_TIPOREL = xconfig.ID_TIPOREL
                                       where xconfig.ACTIVO=1 
 
                                ";

                DynamicParameters dp = new DynamicParameters();

                if (IdTipoRel != -1)
                {
                    query += IdTipoRel == -1 ? "" : " and tiporel.ID_TIPOREL = @_IdTipoRel ";
                    dp.Add("@_IdTipoRel", IdTipoRel);
                }

                reports = sqlConnection.Query<CLS_REPORTS>(query, dp, commandTimeout: 10000).ToList();
                //string rtn = JsonConvert.SerializeObject(teste, Formatting.Indented);
                sqlConnection.Close();
            }

            if (reports.Count == 0)
                return;

            //
            foreach (CLS_REPORTS report in reports)
            {
                GrerarReport(report);
            }



            //foreach (ReprotConfig r in ReprotConfig.getconfigs())
            //    GrerarReport(r);

        }

        private static void GrerarReport(CLS_REPORTS rconf)
        {
            //Byte[] tresults = new Byte[1];

            //bool t = upload(rconf, tresults,"");

            //if(t)
            //{
           // SendEmail(rconf);
            //}

            try
            {
                ReportExecutionServiceSoapClient rs = new ReportExecutionServiceSoapClient();
                rs.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;

                //Prepare Render arguments  
                string reportPath = @"/insqlreports/" + rconf.COD_REPORT.Replace(System.Environment.NewLine, "");
                string historyID = null;
                string deviceInfo = String.Empty;
                string extension = String.Empty;
                string encoding = String.Empty;
                string[] streamIDs = null;

                ExecutionInfo execInfo = new ExecutionInfo();
                TrustedUserHeader trusteduserHeader = new TrustedUserHeader();

                ExecutionHeader execHeader = new ExecutionHeader();
                ServerInfoHeader serviceInfo = new ServerInfoHeader();

                rs.LoadReport(trusteduserHeader, reportPath, historyID, out serviceInfo, out execInfo);

                string format = "PDF";
                string mimeType = "application/PDF";
                Warning[] warnings = null;
                Byte[] results;

                execHeader.ExecutionID = execInfo.ExecutionID;

                List<ParameterValue> parameters = new List<ParameterValue>();

                if (rconf.ID_TIPOREL <= 2)
                {

                    //simular 2016
                    DateTime now = DateTime.Now;
                    string CODYEAR = now.Year.ToString();

                    string CODMONTH = now.Month < 10 ? "0" + now.Month.ToString() : now.Month.ToString();

                    if (rconf.ID_TIPOREL == 2)
                    {
                        CODYEAR = (now.Year - 1).ToString();
                        CODMONTH = "12";
                    }
                    else if (rconf.ID_TIPOREL == 1)
                    {
                        DateTime mesant = DateTime.Now.AddMonths(-1);
                        CODYEAR = mesant.Year.ToString();
                        CODMONTH = mesant.Month.ToString();

                    }

                    rconf.DATFIM = CODYEAR + CODMONTH + "01";
                    rconf.DATINI = Convert.ToDateTime(CODYEAR +"-"+ CODMONTH + "-01").AddMonths(-12).ToString("yyyyMMdd");

                    parameters.Add(new ParameterValue { Name = "ID_CLIENT", Value = rconf.ID_CLIENT.ToString().Replace(Environment.NewLine, "") });
                    //parameters.Add(new ParameterValue { Name = "NOM_CLIENT", Value = rconf.NAM_CLIENT });
                    parameters.Add(new ParameterValue { Name = "COD_TEMA_CLI", Value = rconf.COD_TEMA_CLI.Replace(Environment.NewLine, "") });
                    parameters.Add(new ParameterValue { Name = "pais", Value = rconf.COD_COUNTRY.Replace(Environment.NewLine, "") });
                    parameters.Add(new ParameterValue { Name = "LstTemas", Value = rconf.COD_LSTEMAS.Replace(Environment.NewLine, "") });
                    parameters.Add(new ParameterValue { Name = "Language", Value = rconf.COD_LANG.Replace(Environment.NewLine, "") });
                    parameters.Add(new ParameterValue { Name = "CODYEAR", Value = CODYEAR.Replace(Environment.NewLine, "") });
                    parameters.Add(new ParameterValue { Name = "CODMONTH", Value = CODMONTH.Replace(Environment.NewLine, "") });

                }



                ParameterValue[] _pra = parameters.ToArray();

                var rstpara = rs.SetExecutionParameters(execHeader, trusteduserHeader, _pra, "en-us", out execInfo);

                var rstrender = rs.Render(execHeader, trusteduserHeader, format, deviceInfo, out results, out extension,
                   out mimeType, out encoding, out warnings, out streamIDs);



                //String filename = reportPath + @"c:\temp\0xxxx.pdf";
                //String filename = @"c:\temp\" + rconf.cod_f + ".pdf";
                //FileStream stream = File.OpenWrite(filename);
                //stream.Write(results, 0, results.Length);
                //stream.Close();


                //string server = rconf.COD_FTPSERVER;
                //string path = rconf.COD_FTPFOLDER + rconf.NAM_CLIENT + "/" + DateTime.Now.ToString("yyyy");
                //string filepath = server + path + "/" + rconf.NAM_FILE + ".pdf";
                rconf.NAM_FILE = rconf.NAM_FILE + DateTime.Now.ToString("yyyyMM");
                if (upload(ref rconf, results))
                {
                    using (var sqlConnection = new SqlConnection(connDev))
                    {
                        string query = @" insert into inReports.dbo.RS10f_CLIENT (ID_RS10H,COD_FILE)
                                            values (@IDRS10H,@CODFILE)
                                    ";

                        DynamicParameters dp = new DynamicParameters();
                        dp.Add("@IDRS10H", rconf.ID);
                        dp.Add("@CODFILE", rconf.COD_FILE_LINK.Replace("ftp", "http"));

                        sqlConnection.Open();

                        sqlConnection.Execute(query, dp, commandTimeout: 10000);

                        //string rtn = JsonConvert.SerializeObject(emails, Formatting.Indented);
                        sqlConnection.Close();
                    }


                    SendEmail(rconf);


                }
            }
            catch (Exception ex)
            {
                String message = ex.Message.ToString();

            }
        }

        private static bool FtpDirectoryExists(CLS_REPORTS rconf, string dirPath)
        {
            bool IsExists = true;
            try
            {
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(dirPath);
                request.Credentials = new NetworkCredential(rconf.USERNAME, rconf.PASSWORD);
                request.Method = WebRequestMethods.Ftp.PrintWorkingDirectory;

                FtpWebResponse response = (FtpWebResponse)request.GetResponse();
            }
            catch (WebException ex)
            {
                IsExists = false;
            }
            return IsExists;
        }

        private static bool upload(ref CLS_REPORTS rconf, byte[] infileContents)
        {
            try
            {
                string server = rconf.COD_FTPSERVER;
                string path = rconf.COD_FTPFOLDER + rconf.NAM_CLIENT + "/" + DateTime.Now.ToString("yyyy");
                string auxpath = "";
                rconf.COD_FILE_LINK = server + path + "/" + rconf.NAM_FILE  + ".pdf";

                foreach (string str in path.Split('/'))
                {
                    auxpath += "/" + str;

                    FtpWebRequest create = (FtpWebRequest)WebRequest.Create(server + auxpath);
                    try
                    {
                        create.Method = WebRequestMethods.Ftp.MakeDirectory;
                        create.Credentials = new NetworkCredential(rconf.USERNAME, rconf.PASSWORD);
                        var resp = (FtpWebResponse)create.GetResponse();
                    }
                    catch (Exception er)
                    {
                        er.Message.ToString();
                    }
                }

                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(rconf.COD_FILE_LINK);
                request.Method = WebRequestMethods.Ftp.UploadFile;
                request.Credentials = new NetworkCredential(rconf.USERNAME, rconf.PASSWORD);

                // This example assumes the FTP site uses anonymous logon.

                // Copy the contents of the file to the byte array.
                byte[] fileContents = infileContents;
                request.ContentLength = fileContents.Length;

                //Upload File to FTPServer
                Stream requestStream = request.GetRequestStream();
                requestStream.Write(fileContents, 0, fileContents.Length);
                requestStream.Close();
                FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                response.Close();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        static string UppercaseFirst(string s)
        {
            // Check for empty string.
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }
            // Return char and concat substring.
            return char.ToUpper(s[0]) + s.Substring(1);
        }
        private static bool SendEmail(CLS_REPORTS rconf)
        {
            CLS_KPIS kpis = new CLS_KPIS();

            try
            {
                //Gerar o KPIS
               

                using (var sqlConnection = new SqlConnection(conn))
                {
                    sqlConnection.Open();

                    string query = @" exec iadvisers.dbo.SP_RPT_KPIS @COD_TEMA_CLI,@LST_PAIS,@PERICI,@DATINI,@DATFIM,@LSTTEMAS ";

                    DynamicParameters dp1 = new DynamicParameters();
                    dp1.Add("@LSTTEMAS", rconf.COD_LSTEMAS.Replace(Environment.NewLine, ""));
                    dp1.Add("@COD_TEMA_CLI", rconf.COD_TEMA_CLI.Replace(Environment.NewLine, ""));
                    dp1.Add("@LST_PAIS", rconf.COD_COUNTRY.Replace(Environment.NewLine, ""));
                    dp1.Add("@PERICI", rconf.ID_TIPOREL == 1 ? 0 : 3);
                    dp1.Add("@DATINI", rconf.DATINI);
                    dp1.Add("@DATFIM", rconf.DATFIM);
                    
                    kpis = sqlConnection.Query<CLS_KPIS>(query, dp1, commandTimeout: 10000).SingleOrDefault();


                    //string rtn = JsonConvert.SerializeObject(emails, Formatting.Indented);
                    sqlConnection.Close();
                }

                //get Lista Email a enviar
                IList<CLS_CLIENT_EMAIL> emailtosend = new List<CLS_CLIENT_EMAIL>();
                using (var sqlConnection = new SqlConnection(connDev))
                {
                    string query = @" 
                    select * from inReports.dbo.RS10H_CLIENT_MAIL mail (nolock) where mail.id_RS10H_CLIENT = @ID ";

                    DynamicParameters dp = new DynamicParameters();
                    dp.Add("@ID", rconf.ID);

                    sqlConnection.Open();
                    emailtosend = sqlConnection.Query<CLS_CLIENT_EMAIL>(query, dp, commandTimeout: 10000).ToList();
                    //string rtn = JsonConvert.SerializeObject(emails, Formatting.Indented);
                    sqlConnection.Close();
                }

                CultureInfo ci = rconf.COD_LANG.Replace(Environment.NewLine, "") != "pt" ? new CultureInfo("en-EN") : new CultureInfo("pt-PT");
                DateTime data = Convert.ToDateTime(rconf.DATFIM.Insert(4, "-").Insert(7, "-"));
                string YEARMES = data.ToString("yyyy, ") + UppercaseFirst(data.ToString("MMMM", ci));

                foreach (CLS_CLIENT_EMAIL item in emailtosend)
                {
                    string html = GetHTML(item, rconf, kpis);
                    string subject = rconf.NAM_CLIENT + " | Relatório media relativo a " + (rconf.ID_TIPOREL == 2 ? YEARMES.Substring(0, 4) : YEARMES).ToString();
                    EmailControl(html, subject , item.COD_EMAIL, item.SENDFROM);
                }

                return true;
            }
            catch (Exception er)
            {

                er.Message.ToString();
                return false;
            }
        }

        private static string GetHTML(CLS_CLIENT_EMAIL item, CLS_REPORTS rcof, CLS_KPIS kpis)
        {
            CultureInfo ci = rcof.COD_LANG.Replace(Environment.NewLine, "") != "pt" ? new CultureInfo("en-EN") : new CultureInfo("pt-PT");
            DateTime data = Convert.ToDateTime(rcof.DATFIM.Insert(4, "-").Insert(7, "-"));
            string YEARMES = data.ToString("yyyy, ") + UppercaseFirst(data.ToString("MMMM", ci));

            String html = rcof.COD_LANG.Replace(Environment.NewLine, "") != "pt" ? rcof.HTML_EN : rcof.HTML ;

            html = html.Replace("[N]", kpis.N);
            html = html.Replace("[OTS]", kpis.OTS);
            html = html.Replace("[AVE]", kpis.AVE);
            html = html.Replace("[PRV]", kpis.PRV);
            html = html.Replace("[SHARE]", kpis.SHARE + "%");
            html = html.Replace("[NOME]", item.NAME);
            html = html.Replace("[NOM_CLI]", rcof.NAM_CLIENT);
            html = html.Replace("[ANOMES]", rcof.ID_TIPOREL == 2 ? rcof.DATFIM.Substring(0,4) : YEARMES);
            html = html.Replace("[FILENAMEURL]", rcof.COD_FILE_LINK.Replace("ftp", "http"));
            html = html.Replace("[FILENAME]", rcof.NAM_FILE.TrimEnd());

            return html;
        }

        private static void EmailControl(String body, String subject, string TO, int sendFrom)
        {
            try
            {
                #region email control

                MailMessage myMessage = new MailMessage("info@insigte.com", "info@insigte.com");
                myMessage.IsBodyHtml = true;
                myMessage.Priority = MailPriority.Normal;
                myMessage.Body = body;
                myMessage.Bcc.Add(TO);

                //myMessage.Bcc.Add("jose.moreira@insigte.info");
                //myMessage.Bcc.Add("janete.bento@insigte.info");
                //myMessage.Bcc.Add("luis.mota@insigte.ingo");
                myMessage.Subject = subject;

                SmtpClient mySmtpClient = new SmtpClient();

                if (sendFrom == 0)//Google
                {
                    mySmtpClient = new SmtpClient("smtp.gmail.com", Convert.ToInt32(587));
                    mySmtpClient.Credentials = new NetworkCredential("info@insigte.info", "INfo2020");
                    mySmtpClient.EnableSsl = true;
                }
                else if (sendFrom == 1)//paranomix
                {
                    mySmtpClient = new SmtpClient("panoramix.insigte.com");
                    mySmtpClient.Credentials = new NetworkCredential("info@insigte.com", "jQY4qhQk");
                }
                else if (sendFrom == 2)//SendGrid
                {
                    mySmtpClient = new SmtpClient("smtp.sendgrid.net", Convert.ToInt32(587));
                    System.Net.NetworkCredential credentials = new System.Net.NetworkCredential("insigte_prod", "Insigte#2015");
                    mySmtpClient.Credentials = credentials;
                }

                mySmtpClient.Send(myMessage);

                #endregion
            }
            catch (Exception er)
            {
                //AddToFile("ERRO no metodo (EmailControl) erroMsg: " + er.Message.ToString() + " | " + Dts.Variables["CLIENTE_ID"].Value.ToString());
                //er.Message.ToString();

            }
        }

    }

    public class CLS_REPORTS
    {
        [JsonProperty("ID_CONFIG")]
        public int ID_CONFIG { get; set; }
        [JsonProperty("ID_TIPOREL")]
        public int ID_TIPOREL { get; set; }
        [JsonProperty("ID_FTP")]
        public int ID_FTP { get; set; }
        [JsonProperty("COD_REPORT")]
        public string COD_REPORT { get; set; }
        [JsonProperty("ID_CLIENT")]
        public int ID_CLIENT { get; set; }
        [JsonProperty("NAM_CLIENT")]
        public string NAM_CLIENT { get; set; }
        [JsonProperty("NAM_FILE")]
        public string NAM_FILE { get; set; }
        [JsonProperty("COD_TEMA_CLI")]
        public string COD_TEMA_CLI { get; set; }
        [JsonProperty("COD_LSTEMAS")]
        public string COD_LSTEMAS { get; set; }
        [JsonProperty("COD_LANG")]
        public string COD_LANG { get; set; }
        [JsonProperty("COD_COUNTRY")]
        public string COD_COUNTRY { get; set; }
        [JsonProperty("ID")]
        public int ID { get; set; }
        [JsonProperty("ID_CLIENT_USER")]
        public object ID_CLIENT_USER { get; set; }
        [JsonProperty("COD_FTPSERVER")]
        public string COD_FTPSERVER { get; set; }
        [JsonProperty("COD_FTPFOLDER")]
        public string COD_FTPFOLDER { get; set; }
        [JsonProperty("USERNAME")]
        public string USERNAME { get; set; }
        [JsonProperty("PASSWORD")]
        public string PASSWORD { get; set; }
        [JsonProperty("PERIODO")]
        public string PERIODO { get; set; }
        public string COD_FILE_LINK { get; set; } = "http://www.insigte.com";

        public string DATINI { get; set; } = "20141201";
        public string DATFIM { get; set; } = "20151201";
        public string HTML { get; set; } = "";
        public string HTML_EN { get; set; } = "";
    }

    public class CLS_CLIENT_EMAIL
    {
        [JsonProperty("ID_EMAIL")]
        public int ID_EMAIL { get; set; }
        [JsonProperty("ID_RS10H_CLIENT")]
        public int ID_RS10H_CLIENT { get; set; }
        [JsonProperty("COD_EMAIL")]
        public string COD_EMAIL { get; set; }
        [JsonProperty("SENDFROM")]
        public int SENDFROM { get; set; }
        [JsonProperty("NAME")]
        public string NAME { get; set; }
    }

    public class CLS_KPIS
    {
        public string N { get; set; }
        public string OTS { get; set; }
        public string AVE { get; set; }
        public string PRV { get; set; }
        public string SHARE { get; set; }
    }

}
